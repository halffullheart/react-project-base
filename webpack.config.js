var webpack = require('webpack');

module.exports = {
  entry: {
    app: ['webpack/hot/dev-server', './index.js']
  },
  output: {
    path: './build',
    filename: 'bundle.js'
  },
  cssnext: {
    browsers: "last 2 versions",
  },
  module: {
    loaders: [
      {
        test:     /\.css$/,
        loader:   'style-loader!css-loader!cssnext-loader'
      },
      {
        test:     /\.js$/,
        exclude:  /node_modules/,
        loader:   'babel-loader'
      }
    ]
  }
};
